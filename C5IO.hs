module C5IO where

import System.IO
import Control.Exception
import System.Directory

{- Exercise 5.1

Write a program that asks the user for his or her name. If the name is one of
Simon, John or Phil, tell the user that you think Haskell is a great programming
language. If the name is Koen, tell them that you think debugging Haskell is fun
(Koen Classen is one of the people who works on Haskell debugging); otherwise,
tell the user that you don't know who he or she is.

Write two different versions of this program, one using if statements, the other
using a case statement.
-}

salute = do
  putStrLn "Hello! What is your name?"
  name <- getLine
  return name

saluteProgrammer = putStrLn "I think Haskell is a great programming language!"
saluteDebugger   = putStrLn "I think debugging Haskell is fun!"
saluteStranger   = putStrLn "I don't know what you are or who you are."

programmers = ["Simon","John","Phil"]
debuggers   = ["Koen"]

checkGuestList :: String -> [String] -> Bool
checkGuestList name list = not (null [ x | x <- list, compare x name == EQ ])

checkProgrammersGuestList name = checkGuestList name programmers
checkDebuggersGuestList   name = checkGuestList name debuggers

whoAreYouIf :: IO()
whoAreYouIf = do
  hSetBuffering stdin LineBuffering
  name <- salute
  if checkProgrammersGuestList name
    then saluteProgrammer
    else if checkDebuggersGuestList name
         then saluteDebugger
         else saluteStranger

whoAreYouCase :: IO()
whoAreYouCase = do
  hSetBuffering stdin LineBuffering
  name <- salute
  case checkGuestList name (programmers ++ debuggers) of
    True  -> case checkProgrammersGuestList name of
               True  -> saluteProgrammer
               False -> saluteDebugger
    False -> saluteStranger


{- Exercise 5.2

Write a program that first asks whether the user wants to read
from a file, write to a file or quit. If the user responds quit,
the program should exit. If he responds read, the program should
ask him for a file name and print that file to the screen (if
the file doesn't exist, the program may crash). If he responds
write, it should ask him for a file name and then ask him for
text to write to the file, with "." signaling completion. All
but the "." should be written to the file.
-}

exercise52 = do
  hSetBuffering stdin LineBuffering
  doLoop

doLoop = do
  putStrLn "Do you wand to [read] a file, [write] a file or [quit]?"
  command <- getLine
  case command of
    "read"  -> do putStrLn "Enter a filename to write:"
                  filename <- getLine
                  doRead filename
                  doLoop
    "write" -> do putStrLn "Enter a filename to read:"
                  filename <- getLine
                  putStrLn "Enter text (dot on a line by itself to end):"
                  doWrite filename
                  doLoop
    "quit"  -> do putStrLn "Goodbye!"
                  return()
    _       -> do putStrLn ("I don't understand the command " ++ command)
                  doLoop

doRead filename = do
  exists <- doesFileExist filename
  if exists
    then bracket
           (openFile filename ReadMode)
           hClose
           (\h -> do contents <- hGetContents h
                     putStrLn contents)
    else putStrLn "Sorry, that file does not exist."

doWrite filename = do
  line <- getLine
  case line of
    "." -> return()
    _   -> do bracket
                (openFile filename AppendMode)
                hClose
                (\h -> hPutStrLn h line)
              doWrite filename
