module C4TypeBasics where

{- Exercise 4.1

Figure out for yourself, and then verify the types of the following expressions,
if they have a type. Also note if the expression is a type error:

  1. 'h':'e':'l':'l':'o':[]  :: [Char]
  2. [5,'a']                 :: error
  3. (5,'a')                 :: Num t => (t, Char)
  4. (5::Int) + 10           :: Int
  5. (5::Int) + (10::Double) :: error
-}


{- Exercise 4.2

Figure out for yourself, and then verify the types of the following expressions,
if they have a type. Also note if the expression is a type error:

  1. snd         :: (a, b) -> b
  2. head        :: [a] -> a
  3. null        :: [a] -> Bool
  4. head . tail :: [c] -> c
  5. head . head :: [[c]] -> c
-}


{- Exercise 4.3

Figure out for yourself, and then verify the types of the following expressions,
if they have a type. Also note if the expression is a type error:

  1. \x -> [x]            :: t -> [t]
  2. \x y z -> (x,y:z:[]) :: t -> a -> a -> (t, [a])
  3. \x -> x + 5          :: Num a => a -> a
  4. \x -> "hello, world" :: t -> [Char]
  5. \x -> x 'a'          :: (Chat -> t) -> t
  6. \x -> x x            :: error
  7. \x -> x + x          :: Num a => a -> a

-}


{- Exercise 4.4

Write a data type declaration for Triple, a type which contains three elements,
all of different types. Write functions tripleFst, tripleSnd and tripleThr to
extract respectively the first, second and third.
-}

data Triple a b c = Triple a b c

tripleFst (Triple a b c) = a
tripleSnd (Triple a b c) = b
tripleThr (Triple a b c) = c


{- Exercise 4.5

Write a datatype Quadruple which holds four elements. However, the first two
elements must be the same type and the last two elements must be the same type.
Write a function firstTwo which returns a list containing the first two elements
and a function lastTwo which returns a list containing the last two elements.
Write type signatures for these functions.
-}

data Quadruple = Quadruple Int Int String String

firstTwo (Quadruple a b c d) = a : b : []
lastTwo (Quadruple a b c d) = c : d : []


{- Exercise 4.6

Write a datatype Tuple which can hold one, two, three or four elements,
depending on the constructor (that is, there should be four constructors, one
for each number of arguments). Also provide functions tuple1 through tuple4
which take a tuple and return Just the value in that position, or Nothing if
the number is invalid (i.e., you ask for the tuple4 on a tuple holding only two
elements).
-}

data Tuple a b c d = OneTuple   a
                   | TwoTuple   a b
                   | ThreeTuple a b c
                   | FourTuple  a b c d

tuple1 :: Tuple w x y z -> Maybe w
tuple1 (OneTuple   a      ) = Just a
tuple1 (TwoTuple   a b    ) = Just a
tuple1 (ThreeTuple a b c  ) = Just a
tuple1 (FourTuple  a b c d) = Just a

tuple2 :: Tuple w x y z -> Maybe x
tuple2 (OneTuple   a      ) = Nothing
tuple2 (TwoTuple   a b    ) = Just b
tuple2 (ThreeTuple a b c  ) = Just b
tuple2 (FourTuple  a b c d) = Just b

tuple3 :: Tuple w x y z -> Maybe y
tuple3 (OneTuple   a      ) = Nothing
tuple3 (TwoTuple   a b    ) = Nothing
tuple3 (ThreeTuple a b c  ) = Just c
tuple3 (FourTuple  a b c d) = Just c

tuple4 :: Tuple w x y z -> Maybe z
tuple4 (OneTuple   a      ) = Nothing
tuple4 (TwoTuple   a b    ) = Nothing
tuple4 (ThreeTuple a b c  ) = Nothing
tuple4 (FourTuple  a b c d) = Just d


{- Exercise 4.7

Based on our definition of Tuple from the previous exercise, write a function
which takes a Tuple and returns either the value (if it's a one-tuple), a
Haskell-pair (i.e., (’a’,5)) if it's a two-tuple, a Haskell-triple if it's a
three-tuple or a Haskell-quadruple if it's a four-tuple. You will need to use
the Either type to represent this.
-}

valueOrTuple :: Tuple a b c d -> Either (Either a (a, b)) (Either (a, b, c) (a, b, c, d))
valueOrTuple (OneTuple   a      ) = Left  (Left a)
valueOrTuple (TwoTuple   a b    ) = Left  (Right (a, b))
valueOrTuple (ThreeTuple a b c  ) = Right (Left (a, b, c))
valueOrTuple (FourTuple  a b c d) = Right (Right (a, b, c, d))


{- Exercise 4.8

Write functions listHead, listTail, listFoldl and listFoldr which are
equivalent to their Prelude twins, but function on our List datatype.
Don't worry about exceptional conditions on the first two.
-}

data List a = Nil
            | Cons a (List a)

-- λ> aList = Cons 3 (Cons 8 (Cons 12 (Cons 5 Nil)))

listHead :: List a -> a
listHead (Cons x xs) = x

listTail :: List a -> a
listTail (Cons x Nil) = x
listTail (Cons x xs) = listTail xs

listFoldl fn n (Cons x Nil) = fn n x
listFoldl fn n (Cons x xs)  = listFoldl fn (fn n x) xs

listFoldr fn n (Cons x Nil) = fn n x
listFoldr fn n (Cons x xs)  = fn n (listFoldr fn x xs)


{- Exercise 4.9

Write a function elements which returns the elements in a BinaryTree in a
bottom-up, left-to-right manner (i.e., the first element returned is the
left-most leaf, followed by its parent's value, followed by the other
child's value, and so on). The result type should be a normal Haskell list.
-}

data BinaryTree a = Leaf a
                  | Branch (BinaryTree a) a (BinaryTree a)

-- λ> bin = Branch (Branch (Leaf 1) 12 (Leaf 2)) 42 (Branch (Leaf 7) 76 (Leaf 6))

elements :: BinaryTree a -> [a]
elements (Leaf a) = a : []
elements (Branch left a right) = (elements left) ++ [a] ++ (elements right)


{- Exercise 4.10

Write a foldr function treeFoldr for BinaryTrees and rewrite elements in terms
of it (call the new one elements2). The type of treeFoldr should be
(a -> b -> b) -> b -> BinaryTree a -> b.

Write a foldl function treeFoldl for BinaryTrees and rewrite elements in terms
of it (call the new one elements3).
-}

treeFoldr :: (a -> b -> b) -> b -> BinaryTree a -> b
treeFoldr fn n (Leaf a) = fn a n
treeFoldr fn n (Branch left a right) = treeFoldr fn (fn a (treeFoldr fn n right)) left

elements2 :: BinaryTree a -> [a]
elements2 a = treeFoldr (:) [] a

treeFoldl :: (b -> a -> b) -> b -> BinaryTree a -> b
treeFoldl fn n (Leaf a) = fn n a
treeFoldl fn n (Branch left a right) = treeFoldl fn (fn (treeFoldl fn n left) a) right

elements3 :: BinaryTree a -> [a]
elements3 a = treeFoldl (\ x y -> x ++ [y]) [] a


{- Exercise 4.11

Test whether the CPS-style fold mimics either of foldr and foldl. If not, where
is the difference?
-}

cfold' f z [] = z
cfold' f z (x:xs) = f x z (\y -> cfold' f y xs)

cfold f z l = cfold' (\x t g -> f x (g t)) z l

{-
foldl (-) 6 [0,1,2,3,4]
=> (((((6 - 0) - 1) - 2) - 3) - 4)
=> -4

foldr (-) 6 [0,1,2,3,4]
=> (0 - (1 - (2 - (3 - (4 - 6)))))
=> -4

cfold (-) 6 [0,1,2,3,4]
=> cfold' (\x t g -> (-) x (g t)) 6 [0,1,2,3,4]
=> (\x t g -> (-) x (g t)) 0 6 (\y -> cfold' (\x t g -> (-) x (g t)) y [1,2,3,4])
=> 0 - (cfold' (\x t g -> (-) x (g t)) 6 [1,2,3,4])
=> 0 - ((\x t g -> (-) x (g t)) 1 6 (\y -> cfold' (\x t g -> (-) x (g t)) y [2,3,4]))
=> 0 - (1 - (cfold' (\x t g -> (-) x (g t)) 6 [2,3,4]))
=> 0 - (1 - (2 - (cfold' (\x t g -> (-) x (g t)) 6 [3,4])))
=> 0 - (1 - (2 - (3 - (4 - 6))))
=> -4

The CPS fold mimics the `foldr' function.
-}


{- Exercise 4.12

Write map and filter using continuation passing style.
-}

--cmap' :: (f -> xs -> x) -> xs -> (f -> xs -> x)
-- cmap' f l [] = l
-- cmap' f l (x:xs) = cmap' f  ((f x):l) xs

-- cmap f xs = cmap' f [] xs

-- cmap f xs = cmap' (\x t g -> f)  
