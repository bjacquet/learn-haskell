Learn Haskell
=============

My take on the exercises from the book [Yet Another Haskell Tutorial]
(https://en.wikibooks.org/wiki/Yet_Another_Haskell_Tutorial).
