module C3LanguageBasics where

import Data.Char
import System.IO

-- Exercise 3.2 Use a combinaion of fst and snd to extract the character out
-- of the tuple ((1,'a'),"foo").

ex2 = snd (fst ((1,'a'),"foo"))


-- Exercise 3.3 Use map to convert a string into a list of booleans, each
-- element in the new list representing whether or not the original element was
-- a lower-case character. That is, it should take the string "aBCde" and
-- return [True,False,False,True,Ture].

ex3 = map (Data.Char.isLower) "aBCde"


-- Exercise 3.4 Use the functions mentioned in this section (you will need two
-- of them) to compute the number of lower-case letters in a string. For
-- instance, on "aBCde" it should return 3.

ex4Aux char = if Data.Char.isLower char
                then 1
                else 0

ex4 = foldl (+) 0 (map (ex4Aux) "aBCde")


-- Exercise 3.5 Given that the function max returns the maximum of two numbers,
-- write a function using a fold that will return the maximum value in a list
-- (and zero if the list is empty). So, when applied to [5,10,2,8,1] it will
-- return 10. Assume that the values in the list are always >= 0. Explain to
-- yourself why it works.

ex5 = foldl (max) (0) [5,10,2,8,1]


-- Exercise 3.6 Write a function that takes a list of pairs of length at least
-- 2 and returns the first component of the second element in the list. So,
-- when provided with [(5,'b'),(1,'c'),(6,'a')], it will return 1.

ex6 = fst (head (tail [(5,'b'),(1,'c'),(6,'a')]))


-- Exercise 3.7 The fibonacci sequence is defined by:
-- F_n = \begin{cases} 1 & n = 1 \mbox{ or } n = 2 \\ F_{n-2} + F_{n-1} & \mbox{otherwise} \\ \end{cases}
-- Write a recursive function fib that takes a positive integer n as a parameter
-- and calculates F_n.

fib 1 = 1
fib 2 = 2
--fib n = fib (n-2) + fib (n-1)
--fib n = foldl (+) 0 [fib (n-2), fib (n-1)]
fib n = foldl (+) 0 (map (fib) [n-2,n-1])

ex7 num = fib num


-- Exercise 3.8 Define a recursive function mult that takes two positive
-- integers a and b and returns a*b, but only uses addition (i.e., no fair
-- just using multiplication). Begin by making a mathematical definition in
-- the style of the previous exercise and the rest of this section.

mult a 0 = 0
mult 0 b = 0
mult a 1 = a
mult 1 b = b
mult a b = a + mult a (b - 1)

ex8 a b = mult a b


-- Exercise 3.9 Define a recursive function my_map that behaves identically to
-- the standard function map.

my_map func [] = []
my_map func (x:xs) = func x : my_map func xs

ex9 func xs = my_map func xs


-- Exercise 3.10 Write a program that will repeatedly ask the user for numbers
-- until she types in zero, at which point it will tell her the sum of all the
-- numbers, the product of all the numbers, and, for each number, its factorial.

askForNumbers = do
  hSetBuffering stdin LineBuffering
  putStrLn "Give me a number (or 0 to stop):"
  input <- getLine
  let number = read input :: Int
  if number == 0
    then return []
    else do
         moreNumbers <- askForNumbers
         return (number : moreNumbers)

my_sum n = foldl (+) 0 n

my_product n = foldl (mult) 1 n

factorial 1 = 1
factorial n = n * factorial (n - 1)

printFactorial [] = return ()
printFactorial (x:xs) = do
  putStrLn ((show x) ++ " factorial is " ++ (show (factorial x)))
  printFactorial xs

ex10 = do
  numbers <- askForNumbers
  putStrLn ("The sum is " ++ (show (my_sum numbers)))
  putStrLn ("The product is " ++ (show (my_product numbers)))
  printFactorial numbers